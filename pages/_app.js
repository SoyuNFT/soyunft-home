import { ChakraProvider } from '@chakra-ui/react'
import '../styles.scss';

function MyHome({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  )
}

export default MyHome
