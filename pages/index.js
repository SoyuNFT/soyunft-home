import data from '../Utils/data';

import LinksBanner from '../components/Home/LinksBanner/'
import Preview from '../components/Home/Preview'
import Roadmap from '../components/Home/Roadmap'
import Tokenomics from '../components/Home/Tokenomics'
import Whoweare from '../components/Home/Whoweare'
import Footer from '../layout/Footer';
import Header from '../layout/Header';
import Main from '../modules/Main';

export default function Home() {
  return (
    <div className="app">
      <Header/>
      <Main/>
      <Footer/>
    </div>
  )
}