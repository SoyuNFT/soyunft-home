import data from '../Utils/data';
import LinksBanner from '../components/Home/LinksBanner';
import Roadmap from '../components/Home/Roadmap';
import Tokenomics from '../components/Home/Tokenomics';
import Whoweare from "../components/Home/Whoweare";
import Footer from '../layout/Footer';
import Header from '../layout/Header';

export default function quests() {
  return (
    <div className="quests">
      <Header/>
      Quests
      <Footer/>
    </div>
  )
}