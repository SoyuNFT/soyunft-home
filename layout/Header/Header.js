import styles from './header.module.scss'
import Image from 'next/image'
import data from '../../Utils/data';
import Link from 'next/link';

export default function Header() {
    return (
        <div className={styles.header}>
            <div className={styles.container}>
                <div className={styles.header_left}>
                    <svg xmlns="http://www.w3.org/2000/svg" className={styles.icon_home} viewBox="0 0 20 20" fill="currentColor">
                        <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
                    </svg>
                    <div className={styles.header_txt}><Link href="/"><a>WHITEPAPER</a></Link></div>
                    <div className={styles.header_txt}><Link href="/"><a>ROADMAP</a></Link></div>
                    <div className={styles.header_txt}><Link href="/"><a>ABOUT US</a></Link></div>
                </div>
                <div className={styles.header_right}>
                    <div className={styles.name}>
                        <Link href='/'><a>Soyu</a></Link>
                    </div>
                </div>
            </div>
        </div>
    )
}