import styles from './footer.module.scss'
import Image from 'next/image'
import data from '../../Utils/data';
import  twi from '../../Public/img/logo_twitter.svg'
import disc from '../../Public/img/logo_discord.svg'
export default function Footer() {
    return (
        <div className={styles.container}>
            <div className={styles.footer}>
                <div className={styles.icons}>
                    <Image 
                        src={twi}
                        alt="twitter logo"
                        width="42px"
                        height="42px"
                    />
                    <Image 
                        src={disc}
                        alt="discord logo"
                        width="42px"
                        height="42px"
                    />
                </div>
                <p>
                    Mention légales<br/> Informations complémentaires<br/> Investissement long terme
                </p>
                <a href='#'></a>
            </div>
        </div>
         
    )
}
  