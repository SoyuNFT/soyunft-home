import styles from './Main.module.scss'
import Image from 'next/image'
import data from '../../Utils/data';
import Preview from '../../components/Home/Preview';
import Whoweare from '../../components/Home/Whoweare';
import Storymap from '../../components/Home/Storymap';
import Roadmap from '../../components/Home/Roadmap';
import LinksBanner from '../../components/Home/LinksBanner';
export default function Main() {
    return (
        <>
            <div style={{height: 90 + "vh"}}>a</div>
            <Preview/>
            <Whoweare/>
            <Storymap/>
            <Roadmap/>
            <LinksBanner/>
        </>
    )
}