const data = {
    team: {
        admin1: {
            job: "Developper",
            age: "21",
            percentage: "20%"
        },
        admin2: {
            job: "Designer",
            age: "23",
            percentage: "20%"
        },
        admin3: {
            job: "Developper",
            age: "22",
            percentage: "20%"
        },
    },
    roadmap:{

    },
    links: {

    },
    tokenomics: {
        total_supply: 5000,
        whitelist_supply: 2000,
    },
}

module.exports = data;