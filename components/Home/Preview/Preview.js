import styles from './Preview.module.scss'
import data from '../../../Utils/data';
import Image from 'next/image'
export default function Preview() {
    return (
        <div>
            <div className={styles.container}>
                <div className={styles.preview_right}>
                    <div className={styles.wcontain}>
                        <div className={styles.img_blur}>
                            <div className={styles.img1}></div>
                            <div className={styles.img2}></div>
                            <div className={styles.img3}></div>
                        </div>
                        <div className={styles.div_txt}>
                            <h3 className={styles.title3_preview}>Lorem Ipsum is simply dummy text of the printing</h3>
                            <p className={styles.p_preview}>
                                Lorem Ipsum is simply dummy text of the printing and 
                                typesetting industry. Lorem Ipsum has been the industry's
                                standard dummy text ever since the 1500s, when an unknown
                                printer took a galley.
                            </p>
                        </div> 
                    </div>
                </div>
            </div>
            <div className={styles.imgtransition}></div>
        </div>
        
    )
}