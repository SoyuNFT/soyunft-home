import styles from './LinksBanner.module.scss'
import Image from 'next/image';
import data from '../../../Utils/data';
import imgleft from '../../../Public/img/img_left.png'
import imgmiddle from '../../../Public/img/img_middle.png'
import imgright from '../../../Public/img/img_right.png'
export default function LinksBanner() {
    return (
        <div className={styles.linksbanner}>
            <div className={styles.container}>
                <Image
                    className={styles.imgleft} 
                    src={imgleft}
                    alt="nft"
                    width='450px'
                />
                <Image
                    className={styles.imgmiddle} 
                    src={imgmiddle}
                    alt="nft"
                    width='400px'
                />
                <Image
                    className={styles.imgright} 
                    src={imgright}
                    alt="nft"
                    width='450px'
                />
            </div>
        </div>
        
    )
}