import styles from './Storymap.module.scss';
import Image from 'next/image';
import data from '../../../Utils/data';

export default function Storymap() {
    return (
        <div className={styles.container}>
            <div className={styles.text}>
                <h2>
                    STORYMAP
                </h2>
                <p>
                    Lorem Ipsum is simply dummy text of the printing 
                    and typesetting industry. Lorem Ipsum has been 
                    the industry's standard dummy text ever since 
                    the 1500s, when an unknown printer took a galley 
                    of type and scrambled it to make a type specimen 
                    book.
                </p>
            </div>
            
        </div>
    )
}