import styles from './Roadmap.module.scss'
import data from '../../../Utils/data';
import Image from 'next/image';

export default function Roadmap() {
    return (
        <div className={styles.container}>
            <div className={styles.items}>
                <div className={styles.roadmap_h2}>
                    <h2 className={styles.title2_roadmap}>ROADMAP</h2>
                </div>
                <div className={styles.img_roadmap_1}>
                    <h3 className={styles.img_h3}>PRINT</h3>
                </div>
                <div className={styles.img_roadmap_2}>
                    <h3 className={styles.img_h3}>LOOT</h3>
                </div>
                <div className={styles.img_roadmap_3}>
                    <h3 className={styles.img_h3}>DROP</h3>
                </div>
            </div>
            
        </div>
    )
}