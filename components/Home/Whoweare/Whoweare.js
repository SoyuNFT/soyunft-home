import styles from './Whoweare.module.scss'
import Image from 'next/image';
import data from '../../../Utils/data';
import MemberCard from './MemberCard';

export default function Whoweare() {
    return (
        <div className={styles.container}>
            <div className={styles.txt}>
                <h2 className={styles.title2_whoweare}>QUI SOMMES NOUS</h2>
                <p className={styles.p_whoweare}>
                    Lorem Ipsum is simply dummy text of 
                    the printing and typesetting industry.
                    Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, 
                    when an unknown printer took a galley of 
                    type and scrambled it to make a type 
                    specimen book. It has survived not only 
                    five centuries, but also the leap into 
                    electronic typesetting, remaining essentially 
                    unchanged. 
                </p>
            </div>
            
            <div className={styles.container_img_whoweare}>
                <MemberCard name="Koyu" desc="Founder, Artist director, Main Illustrator, Designer" link="NFT1"/>
                <MemberCard name="Kyo" desc="Community manager, Main scriptwriter" link="NFT2"/>
                <MemberCard name="Kozu" desc="Web3 developer" link="NFT1"/>
                <MemberCard name="Kazo" desc="Assistant illustrator" link="NFT2"/>
                <MemberCard name="Koyu" desc="Motion designer" link="NFT1"/>
            </div>
        </div>
    )
}