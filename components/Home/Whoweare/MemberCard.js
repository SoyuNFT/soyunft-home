import styles from './Membercard.module.scss'
import Image from 'next/image';
import data from '../../../Utils/data';
import NFT1 from '../../../Public/img/NFT1.png'
import NFT2 from '../../../Public/img/NFT2.jpg'
export default function MemberCard(props) {
    return(
        <div className={styles.container}>
            <Image 
                className={styles.img}
                src={NFT2}
                alt="nft"
                width="382px"
                height="382px"
            />
            <div className={styles.txt_container}>
                <h3>{props.name}</h3>
                <p>{props.desc}</p>
            </div>
        </div>
    )
    
}
